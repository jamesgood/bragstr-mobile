$(document).ready(function () {

  // Notification Dismissal
  $('.notification-box span.close').click(function () {
    $('.notification-box').animate({
      'margin-left' : '-120%'
    }, 200, 'swing');
  });

  // Toggles active class on selected picks
  $('.sportsbook-wrapper td:not(.total):not(:first-of-type), .sportsbook-wrapper td.total span').click(function () {
    var button = $(this);
    button.toggleClass('active');
  });
  // Sportsbook - Load more animation/effect
  $('a.load-more').click(function (e) {
    e.preventDefault();
    var button;
    button = $(this);
    button.addClass('ajax-load');
    setTimeout(function () { // Fake a loading time and response
      button.hide();
      button.parent().find('.hidden').fadeIn();
    }, 1000);
  });

  // Sportsbook > Betslip class toggle (for animation)
  $('.pick .button, .back-to-sportsbook').click(function (e) {
    e.preventDefault();
    $('#sportsbook-wrapper').toggleClass('flipped');
  });

  // Betslip - load more animation/effect
  $('.bet-slip a.more').click(function (e) {
    e.preventDefault();
    $('#sportsbook-wrapper .bet-slip .bankroll').toggleClass('open');
    var text;
    text = $(this).text();
    $(this).text(text == "+ Show More" ? "- Show Less" : "+ Show More");
  });
  $('.sidebar-nav').css({
    'height' : window.outerHeight
  });
  $('.betslip-select td.lock a').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('selected');
  });

  $('.sidebar-toggle').click(function () {
    if ($('.sidebar-nav').hasClass('open')) {
      $('body').css('overflow', 'auto');
      $('.sidebar-nav').removeClass('open');
      $('.sidebar-nav').addClass('closed');
      $('.body-container').animate({'opacity' : '1'});

    } else {
      $('.sidebar-nav').removeClass('closed');
      $('.sidebar-nav').addClass('open');
      $('.body-container').animate({'opacity' : '0.1'});
      $('body').css('overflow', 'hidden');

      // // Disable scrolling
      // document.ontouchstart = function(e){
      //   e.preventDefault();
      // }
    }
  })
});